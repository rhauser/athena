/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page PanTauExample_page PanTauExample Package

This package contains job option fragments for PanTau.

@author Peter Wienemann <peter.wienemann@cern.ch>
@author Sebastian Fleischmann <Sebastian.Fleischmann@cern.ch>

@section PanTauExample_PanTauExampleIntro Introduction

Job option fragments for PanTau.



*/
